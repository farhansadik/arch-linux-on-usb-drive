# Install Arch Linux in USB

Ever thought of using any computer which is not yours, with all your personal stuff and configuration? It is possible with any Linux distribution. Yes! You can use your own, customized Linux OS on any machine with just a USB drive.

### Things to do before installation 
<hr/>

> Note: At least 4 GB of storage space is recommended. A modest set of packages will fit, leaving a little free space for storage.

There are various ways of installing Arch on removable media, depending on the operating system you have available:

* If you have another Linux computer available (it need not be Arch), you can follow the instructions at [Install from existing Linux](https://wiki.archlinux.org/index.php/Install_Arch_Linux_from_existing_Linux).
* An Arch Linux CD/USB can be used to install Arch onto the removable medium, via booting the CD/USB and following the [installation guide](https://gitlab.com/farhansadik/arch-linux-installation-guide). If booting from a Live USB, the installation cannot be made to the same removable medium you are booting from.
* If you run Windows or macOS, download VirtualBox, install VirtualBox Extensions, attach your removable medium to a virtual machine running Linux (either already installed or via a live ISO), and point the installation into the now attached drive while using the instructions at the [Installation guide](https://gitlab.com/farhansadik/arch-linux-installation-guide).

### Pre-Installation

<hr/>

This tutorial is all about installing Arch Linux on your usb drive ( fully reconfigurable personalized OS, NOT just a Live USB ), customize it, and use it on any PC you have access to. So let’s gets started.

**Connecting to the Internet**

```
# ping google.com 
```

If ping doesn't work, you'll have to manually configure it. Enter your interface with the `dhcpcd` command and hope to get connected. And if you're using wired broadband connection then try with [`pppd`](https://wiki.archlinux.org/index.php/Ppp).

> **Note:** In the installation image, [systemd-networkd](https://wiki.archlinux.org/index.php/Systemd-networkd), [systemd-resolved](https://wiki.archlinux.org/index.php/Systemd-resolved) and [iwd](https://wiki.archlinux.org/index.php/Iwd) are preconfigured and enabled by default. That will not be the case for the installed system.

### Partitioning Disk

<hr/>

> **Note**: While this procedure will not cause loss of data, some users have experienced changes to their internal drive’s bootup behavior depending on Linux distributions selected. To prevent any possibility of this occurrence, you may wish to disconnect your hard drive before continuing with the USB install portion of the tutorial.

When recognized by the live system, disks are assigned to a [block device](https://wiki.archlinux.org/index.php/Block_device) such as `/dev/sda`, `/dev/nvme0n1` or `/dev/mmcblk0`. To identify these devices, use [`lsblk`](https://wiki.archlinux.org/index.php/Lsblk) or *fdisk*.

We will now create the partitions on the hard disk for the OS installation. First, list the available disks using the fdisk command.

```
# fdisk -l /dev/sdX
```

> Note: If the device is not specified, *fdisk* will list all partitions in `/proc/partitions`.

The following [partitions](https://wiki.archlinux.org/index.php/Partition) are **required** for a chosen device:

- One partition for the [root directory](https://en.wikipedia.org/wiki/Root_directory) `/`.
- For booting in [UEFI](https://wiki.archlinux.org/index.php/UEFI) mode: an [EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition).

> **Note:** We're going through uefi installation here, to make it compatible with any available PC. 

**Creating Partition Table**

> **Warning:** Creating a new partition table will erase entire disk.

Start *fdisk* against your drive as root.

```
# fdisk /dev/sdX
```

To create a new partition table and clear all current partition data type **o** at the prompt for a MBR partition table or **g** for a GUID Partition Table (GPT). Skip this step if the table you require has already been created.

> Remember we're going for uefi, so uefi needs GPT partition. Go for **g**

The first sector must be specified in absolute terms using sector numbers. The last sector can be specified using the absolute position in sectors or using the **+** symbol to specify a position relative to the start sector measured in sectors, kibibytes (**K**), mebibytes (**M**), gibibytes (**G**), tebibytes (**T**), or pebibytes (**P**); for instance, setting **+2G** as the last sector will specify a point 2GiB after the start sector. Pressing the **Enter** key with no input specifies the default value, which is the start of the largest available block for the start sector and the end of the same block for the end sector.

>  **Note for BIOS**: BIOS is not required *efi* partition. You may proceed without creating *efi* partition.

>  **Note for UEFI:** The *EFI system partition (ESP)*, a small partition formatted with FAT32, is usually around 500MB, this is where the EFI boot loaders and applications used by the firmware at system during start-up are stored. If your hard drive is in the *GUID Partition table (GPT)* partition style, it will automatically generate an EFI system partition after you have installed your operating systems. Both Windows and Mac operating systems are supported.

- Minimum Partition Size *At least 260 MiB*
- [EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition) requires type `EFI System`.
- If the disk from which you want to boot [already has an EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition#Check_for_an_existing_partition), do not create another one, but use the existing partition instead.
- Mount Point `/mnt/boot` or `/mnt/efi` or `/mnt/boot/efi`

***EFI Partition***

![efi_partition](images/efi.png)

> **Note for Swap Partition:** It is recommended to use Linux swap for any swap partitions, since systemd will automount it.

***Root Partition***

![root_partition](images/root.png)

> Write the table to disk and exit via the **w** command.

**Format File System**

Once the partitions have been created, each must be formatted with an appropriate [file system](https://wiki.archlinux.org/index.php/File_system). For example, if the root partition is on `/dev/sdX0` and will contain the **Ext4** file system.

> Check your partition table using `lsblk`.

```
# mkfs.ext4 /dev/sdX0
```

The UEFI specification mandates support for the FAT12, FAT16, and FAT32 file systems. To prevent potential issues with other operating systems and also since the UEFI specification only mandates supporting FAT16 and FAT12 on removable media, it is recommended to use FAT32.

After creating the partition, [format](https://wiki.archlinux.org/index.php/Format) it as [FAT32](https://wiki.archlinux.org/index.php/FAT32). To use the `mkfs.fat` utility, [install](https://wiki.archlinux.org/index.php/Install) [dosfstools](https://www.archlinux.org/packages/?name=dosfstools).

```
# mkfs.fat -F32 /dev/sdX0
```

If you created a partition for **swap**, initialize it with *mkswap*:

```
# mkswap /dev/sdX0
# swapon /dev/sdX0
```

**Mount File System**

[Mount](https://wiki.archlinux.org/index.php/Mount) the root volume to `/mnt`. For example, if the root volume is `/dev/sdX`:

```
# mount /dev/sdX0 /mnt
```

Mount Point for EFI partition `/mnt/efi` or `/mnt/boot/efi` for mounting EFI system partition are:

```
# mkdir -p /mnt/boot/efi
# mount /dev/sdX0 /mnt/boot/efi
```

### Installation

<hr/>

**Update the system clock**

To ensure the system clock is accurate:

```
# timedatectl set-ntp true
```

To check the service status, use `timedatectl status`

**Select the mirrors**

To view world wide server list, please visit [Arch Liux Server List](https://www.archlinux.org/mirrorlist/all/). I'm using *xeonbd* server as Bangladesh mirror server!

```
# vim /etc/pacman.d/mirrorlist
```

Add `Server = http://mirror.xeonbd.com/archlinux/$repo/os/$arch` top of the file. Then save and exit.

**Install essential packages**

```
# pacstrap /mnt base base-devel linux linux-firmware vim
```



### Configure the system

<hr/>

**Fstab**

Generate an [fstab](https://wiki.archlinux.org/index.php/Fstab) file:

```
# genfstab -U /mnt >> /mnt/etc/fstab
```

Check the resulting `/mnt/etc/fstab` file, and edit it in case of errors. For check `# cat /mnt/etc/fstab`.

**Chroot**

[Change root](https://wiki.archlinux.org/index.php/Change_root) into the new system:

```
# arch-chroot /mnt
```

**Time Zone**

```
# ln -sf /usr/share/zoneinfo/Asia/Dhaka /etc/localtime
```

Run `hwclock` to generate `/etc/adjtime`:

```
# hwclock --systohc --utc
```

**Localization**

Uncomment `en_US.UTF-8` and `ISO-8859-1` from selected file, and then save it.

```
# vim /etc/locale.gen
```

Generate locale file:

```
# locale-gen
```

Create the `locale.conf` file, and set the `LANG` variable accordingly:

```
# vim /etc/locale.conf
LANG=en_US.UTF-8
```

**Network Configuration**

Just enter your host name. For example `admin@example ->`, here `admin` is user name and `example` is the host name. Shortcut example `# echo "archlinux" > /etc/hostname`

```
# vim /etc/hostname
```

Add matching entries to `hosts`:

```
# vim /etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   myhostname.localdomain	myhostname
```

If the system has a permanent IP address, it should be used instead of `127.0.1.1`.

**Initramfs**

Before [creating the initial RAM disk](https://wiki.archlinux.org/index.php/Mkinitcpio#Image_creation_and_activation), in `/etc/mkinitcpio.conf` move the `block` and `keyboard` hooks before the `autodetect` hook. This is necessary to allow booting on multiple systems each requiring different modules in early userspace.

```
# vim /etc/mkinitcpio.conf
```

and find HOOKS. 

Example Order: 

```
HOOKS = (base udev block keyboard autodetect modconf filesystems fsck)
```

After that we need to generate initcpio: 

```
# mkinitcpio -p linux 
```



**Boot Loader (Grub)**

A boot loader is a piece of software started by the firmware ([BIOS](https://en.wikipedia.org/wiki/BIOS) or [UEFI](https://wiki.archlinux.org/index.php/UEFI)). It is responsible for loading the kernel with the wanted [kernel parameters](https://wiki.archlinux.org/index.php/Kernel_parameters), and [initial RAM disk](https://wiki.archlinux.org/index.php/Mkinitcpio) based on configuration files. In the case of UEFI, the kernel itself can be directly launched by the UEFI using the EFI boot stub. A separate boot loader or boot manager can still be used for the purpose of editing kernel parameters before booting.

- BIOS

  ```
  # pacman -S grub
  # grub-install --target=i386-pc /dev/sdX --recheck
  # grub-mkconfig -o /boot/grub/grub.cfg
  ```

- UEFI

  > The section assumes you are installing GRUB for x86_64 systems. For IA32 (32-bit) UEFI systems (not to be confused with 32-bit CPUs), replace `x86_64-efi` with `i386-efi` where appropriate.

  ```
  # pacman -S grub efibootmgr 
  # grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --removable --recheck
  # grub-mkconfig -o /boot/grub/grub.cfg
  ```

**Network Manager**

```
# pacman -S networkmanager
```

Automate Network Configuration

```
# systemctl enable NetworkManager.service
```

**Root Password**

Set the root [password](https://wiki.archlinux.org/index.php/Password):

```
# passwd
```

## Reboot

Exit the chroot environment by typing `exit` or pressing `Ctrl+d`. Optionally manually unmount all the partitions with `umount -R /mnt`: this allows noticing any "busy" partitions, and finding the cause with **fuser**.

```
# exit
# umount -R /mnt
# reboot
```

Finally, restart the machine by typing `reboot`: any partitions still mounted will be automatically unmounted by systemd. Remember to remove the installation medium and then login into the new system with the root account.

## Post-installation

For to do thing after installation visit ~ https://gitlab.com/farhansadik/arch-linux-installation-guide#to-do-after-install-arch-linux
<!--
<p align="center">
  <img src="images/powered_by_arch.png">
</p>
-->

<img align="center" src="images/powered_by_arch.png">

Source: </br>
1. [Arch Linux Installation Guide](https://gitlab.com/farhansadik/arch-linux-installation-guide)
2. [Install Arch Linux on a removable medium - ArchWiki](https://wiki.archlinux.org/index.php/Install_Arch_Linux_on_a_removable_medium)
3. [How to Install Linux OS on USB Drive and Run it On Any PC (tecmint.com)](https://www.tecmint.com/install-linux-os-on-usb-drive/)
